package com.xhx.diary.mapper.auto;

import com.xhx.diary.model.auto.BusCustomer;
import com.xhx.diary.model.auto.BusCustomerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 客户 BusCustomerMapper
 * @author xhx_自动生成
 * @email 1587211986@qq.com
 * @date 2020-12-05 00:14:26
 */
public interface BusCustomerMapper {
      	   	      	      	      	      	      	      	      	      	      	      
    long countByExample(BusCustomerExample example);

    int deleteByExample(BusCustomerExample example);
		
    int deleteByPrimaryKey(String id);
		
    int insert(BusCustomer record);

    int insertSelective(BusCustomer record);

    List<BusCustomer> selectByExample(BusCustomerExample example);
		
    BusCustomer selectByPrimaryKey(String id);
		
    int updateByExampleSelective(@Param("record") BusCustomer record, @Param("example") BusCustomerExample example);

    int updateByExample(@Param("record") BusCustomer record, @Param("example") BusCustomerExample example); 
		
    int updateByPrimaryKeySelective(BusCustomer record);

    int updateByPrimaryKey(BusCustomer record);
  	  	
}