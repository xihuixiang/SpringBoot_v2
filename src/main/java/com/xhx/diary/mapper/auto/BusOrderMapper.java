package com.xhx.diary.mapper.auto;

import com.xhx.diary.model.auto.BusOrder;
import com.xhx.diary.model.auto.BusOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 约拍清单 BusOrderMapper
 * @author xhx_自动生成
 * @email 1587211986@qq.com
 * @date 2020-12-05 00:34:06
 */
public interface BusOrderMapper {
      	   	      	      	      	      	      	      	      	      	      	      
    long countByExample(BusOrderExample example);

    int deleteByExample(BusOrderExample example);
		
    int deleteByPrimaryKey(String id);
		
    int insert(BusOrder record);

    int insertSelective(BusOrder record);

    List<BusOrder> selectByExample(BusOrderExample example);

    List<BusOrder> selectByCreater(String creater);
		
    BusOrder selectByPrimaryKey(String id);
		
    int updateByExampleSelective(@Param("record") BusOrder record, @Param("example") BusOrderExample example);

    int updateByExample(@Param("record") BusOrder record, @Param("example") BusOrderExample example); 
		
    int updateByPrimaryKeySelective(BusOrder record);

    int updateByPrimaryKey(BusOrder record);
  	  	
}