package com.xhx.diary.model.param;

import lombok.Data;

@Data
public class SelectOrderByMonthParam {
    private Integer month;
    private Integer year;

}
