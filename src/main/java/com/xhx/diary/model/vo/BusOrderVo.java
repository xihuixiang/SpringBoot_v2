package com.xhx.diary.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 约拍清单 BusOrder
 *
 * @author xhx_自动生成
 * @email 1587211986@qq.com
 * @date 2020-12-05 00:34:06
 */
@Data
public class BusOrderVo implements Serializable {
    private String title;
    private Date start;
    private String className;

// "title": "标题456",
//		 "start": "2020-05-16T10:30:00",
//		 "end": "2020-05-16T12:30:00",
//		 "className": "red"

}