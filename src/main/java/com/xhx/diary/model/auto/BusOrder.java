package com.xhx.diary.model.auto;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.lang.Integer;

/**
 * 约拍清单 BusOrder 
 * @author xhx_自动生成
 * @email 1587211986@qq.com
 * @date 2020-12-05 00:34:06
 */
 @ApiModel(value="BusOrder", description="约拍清单")
public class BusOrder implements Serializable {

	private static final long serialVersionUID = 1L;
	
		
	/**  **/
	@ApiModelProperty(value = "")
	private String id;
		
	/** 拍摄地点 **/
	@ApiModelProperty(value = "拍摄地点")
	private String shootingAddr;
		
	/** 拍摄时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "拍摄时间")
	private Date shootingTime;
		
	/** 顾客人数 **/
	@ApiModelProperty(value = "顾客人数")
	private Integer customerNum;
		
	/** 道具 **/
	@ApiModelProperty(value = "道具")
	private String props;
		
	/** 主题 **/
	@ApiModelProperty(value = "主题")
	private String theme;
		
	/** 收费 **/
	@ApiModelProperty(value = "收费")
	private Integer charge;
		
	/** 拍摄状态（0待拍摄，1已拍摄） **/
	@ApiModelProperty(value = "拍摄状态（0待拍摄，1已拍摄）")
	private Integer status;
		
	/** 创建人 **/
	@ApiModelProperty(value = "创建人")
	private String creater;
		
	/** 创建时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
		
		
	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	 
			
	public String getShootingAddr() {
        return shootingAddr;
    }

    public void setShootingAddr(String shootingAddr) {
        this.shootingAddr = shootingAddr;
    }
	 
			
	public Date getShootingTime() {
        return shootingTime;
    }

    public void setShootingTime(Date shootingTime) {
        this.shootingTime = shootingTime;
    }
	 
			
	public Integer getCustomerNum() {
        return customerNum;
    }

    public void setCustomerNum(Integer customerNum) {
        this.customerNum = customerNum;
    }
	 
			
	public String getProps() {
        return props;
    }

    public void setProps(String props) {
        this.props = props;
    }
	 
			
	public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
	 
			
	public Integer getCharge() {
        return charge;
    }

    public void setCharge(Integer charge) {
        this.charge = charge;
    }
	 
			
	public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
	 
			
	public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }
	 
			
	public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
	 
			
	public BusOrder() {
        super();
    }
    
																																																				
	public BusOrder(String id,String shootingAddr,Date shootingTime,Integer customerNum,String props,String theme,Integer charge,Integer status,String creater,Date createDate) {
	
		this.id = id;
		this.shootingAddr = shootingAddr;
		this.shootingTime = shootingTime;
		this.customerNum = customerNum;
		this.props = props;
		this.theme = theme;
		this.charge = charge;
		this.status = status;
		this.creater = creater;
		this.createDate = createDate;
		
	}
	
}