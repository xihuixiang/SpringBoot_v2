package com.xhx.diary.model.auto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cn.hutool.core.util.StrUtil;
/**
 * 约拍清单 BusOrderExample
 * @author xhx_自动生成
 * @email 1587211986@qq.com
 * @date 2020-12-05 00:34:06
 */
public class BusOrderExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BusOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
				
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
			
        public Criteria andShootingAddrIsNull() {
            addCriterion("shooting_addr is null");
            return (Criteria) this;
        }

        public Criteria andShootingAddrIsNotNull() {
            addCriterion("shooting_addr is not null");
            return (Criteria) this;
        }

        public Criteria andShootingAddrEqualTo(String value) {
            addCriterion("shooting_addr =", value, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrNotEqualTo(String value) {
            addCriterion("shooting_addr <>", value, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrGreaterThan(String value) {
            addCriterion("shooting_addr >", value, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrGreaterThanOrEqualTo(String value) {
            addCriterion("shooting_addr >=", value, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrLessThan(String value) {
            addCriterion("shooting_addr <", value, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrLessThanOrEqualTo(String value) {
            addCriterion("shooting_addr <=", value, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrLike(String value) {
            addCriterion("shooting_addr like", value, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrNotLike(String value) {
            addCriterion("shooting_addr not like", value, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrIn(List<String> values) {
            addCriterion("shooting_addr in", values, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrNotIn(List<String> values) {
            addCriterion("shooting_addr not in", values, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrBetween(String value1, String value2) {
            addCriterion("shooting_addr between", value1, value2, "shootingAddr");
            return (Criteria) this;
        }

        public Criteria andShootingAddrNotBetween(String value1, String value2) {
            addCriterion("shooting_addr not between", value1, value2, "shootingAddr");
            return (Criteria) this;
        }
        
			
        public Criteria andShootingTimeIsNull() {
            addCriterion("shooting_time is null");
            return (Criteria) this;
        }

        public Criteria andShootingTimeIsNotNull() {
            addCriterion("shooting_time is not null");
            return (Criteria) this;
        }

        public Criteria andShootingTimeEqualTo(Date value) {
            addCriterion("shooting_time =", value, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeNotEqualTo(Date value) {
            addCriterion("shooting_time <>", value, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeGreaterThan(Date value) {
            addCriterion("shooting_time >", value, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("shooting_time >=", value, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeLessThan(Date value) {
            addCriterion("shooting_time <", value, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeLessThanOrEqualTo(Date value) {
            addCriterion("shooting_time <=", value, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeLike(Date value) {
            addCriterion("shooting_time like", value, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeNotLike(Date value) {
            addCriterion("shooting_time not like", value, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeIn(List<Date> values) {
            addCriterion("shooting_time in", values, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeNotIn(List<Date> values) {
            addCriterion("shooting_time not in", values, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeBetween(Date value1, Date value2) {
            addCriterion("shooting_time between", value1, value2, "shootingTime");
            return (Criteria) this;
        }

        public Criteria andShootingTimeNotBetween(Date value1, Date value2) {
            addCriterion("shooting_time not between", value1, value2, "shootingTime");
            return (Criteria) this;
        }
        
			
        public Criteria andCustomerNumIsNull() {
            addCriterion("customer_num is null");
            return (Criteria) this;
        }

        public Criteria andCustomerNumIsNotNull() {
            addCriterion("customer_num is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerNumEqualTo(Integer value) {
            addCriterion("customer_num =", value, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumNotEqualTo(Integer value) {
            addCriterion("customer_num <>", value, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumGreaterThan(Integer value) {
            addCriterion("customer_num >", value, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("customer_num >=", value, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumLessThan(Integer value) {
            addCriterion("customer_num <", value, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumLessThanOrEqualTo(Integer value) {
            addCriterion("customer_num <=", value, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumLike(Integer value) {
            addCriterion("customer_num like", value, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumNotLike(Integer value) {
            addCriterion("customer_num not like", value, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumIn(List<Integer> values) {
            addCriterion("customer_num in", values, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumNotIn(List<Integer> values) {
            addCriterion("customer_num not in", values, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumBetween(Integer value1, Integer value2) {
            addCriterion("customer_num between", value1, value2, "customerNum");
            return (Criteria) this;
        }

        public Criteria andCustomerNumNotBetween(Integer value1, Integer value2) {
            addCriterion("customer_num not between", value1, value2, "customerNum");
            return (Criteria) this;
        }
        
			
        public Criteria andPropsIsNull() {
            addCriterion("props is null");
            return (Criteria) this;
        }

        public Criteria andPropsIsNotNull() {
            addCriterion("props is not null");
            return (Criteria) this;
        }

        public Criteria andPropsEqualTo(String value) {
            addCriterion("props =", value, "props");
            return (Criteria) this;
        }

        public Criteria andPropsNotEqualTo(String value) {
            addCriterion("props <>", value, "props");
            return (Criteria) this;
        }

        public Criteria andPropsGreaterThan(String value) {
            addCriterion("props >", value, "props");
            return (Criteria) this;
        }

        public Criteria andPropsGreaterThanOrEqualTo(String value) {
            addCriterion("props >=", value, "props");
            return (Criteria) this;
        }

        public Criteria andPropsLessThan(String value) {
            addCriterion("props <", value, "props");
            return (Criteria) this;
        }

        public Criteria andPropsLessThanOrEqualTo(String value) {
            addCriterion("props <=", value, "props");
            return (Criteria) this;
        }

        public Criteria andPropsLike(String value) {
            addCriterion("props like", value, "props");
            return (Criteria) this;
        }

        public Criteria andPropsNotLike(String value) {
            addCriterion("props not like", value, "props");
            return (Criteria) this;
        }

        public Criteria andPropsIn(List<String> values) {
            addCriterion("props in", values, "props");
            return (Criteria) this;
        }

        public Criteria andPropsNotIn(List<String> values) {
            addCriterion("props not in", values, "props");
            return (Criteria) this;
        }

        public Criteria andPropsBetween(String value1, String value2) {
            addCriterion("props between", value1, value2, "props");
            return (Criteria) this;
        }

        public Criteria andPropsNotBetween(String value1, String value2) {
            addCriterion("props not between", value1, value2, "props");
            return (Criteria) this;
        }
        
			
        public Criteria andThemeIsNull() {
            addCriterion("theme is null");
            return (Criteria) this;
        }

        public Criteria andThemeIsNotNull() {
            addCriterion("theme is not null");
            return (Criteria) this;
        }

        public Criteria andThemeEqualTo(String value) {
            addCriterion("theme =", value, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeNotEqualTo(String value) {
            addCriterion("theme <>", value, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeGreaterThan(String value) {
            addCriterion("theme >", value, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeGreaterThanOrEqualTo(String value) {
            addCriterion("theme >=", value, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeLessThan(String value) {
            addCriterion("theme <", value, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeLessThanOrEqualTo(String value) {
            addCriterion("theme <=", value, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeLike(String value) {
            addCriterion("theme like", value, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeNotLike(String value) {
            addCriterion("theme not like", value, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeIn(List<String> values) {
            addCriterion("theme in", values, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeNotIn(List<String> values) {
            addCriterion("theme not in", values, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeBetween(String value1, String value2) {
            addCriterion("theme between", value1, value2, "theme");
            return (Criteria) this;
        }

        public Criteria andThemeNotBetween(String value1, String value2) {
            addCriterion("theme not between", value1, value2, "theme");
            return (Criteria) this;
        }
        
			
        public Criteria andChargeIsNull() {
            addCriterion("charge is null");
            return (Criteria) this;
        }

        public Criteria andChargeIsNotNull() {
            addCriterion("charge is not null");
            return (Criteria) this;
        }

        public Criteria andChargeEqualTo(Integer value) {
            addCriterion("charge =", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotEqualTo(Integer value) {
            addCriterion("charge <>", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeGreaterThan(Integer value) {
            addCriterion("charge >", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeGreaterThanOrEqualTo(Integer value) {
            addCriterion("charge >=", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLessThan(Integer value) {
            addCriterion("charge <", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLessThanOrEqualTo(Integer value) {
            addCriterion("charge <=", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLike(Integer value) {
            addCriterion("charge like", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotLike(Integer value) {
            addCriterion("charge not like", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeIn(List<Integer> values) {
            addCriterion("charge in", values, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotIn(List<Integer> values) {
            addCriterion("charge not in", values, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeBetween(Integer value1, Integer value2) {
            addCriterion("charge between", value1, value2, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotBetween(Integer value1, Integer value2) {
            addCriterion("charge not between", value1, value2, "charge");
            return (Criteria) this;
        }
        
			
        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(Integer value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(Integer value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
        
			
        public Criteria andCreaterIsNull() {
            addCriterion("creater is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIsNotNull() {
            addCriterion("creater is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterEqualTo(String value) {
            addCriterion("creater =", value, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterNotEqualTo(String value) {
            addCriterion("creater <>", value, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterGreaterThan(String value) {
            addCriterion("creater >", value, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterGreaterThanOrEqualTo(String value) {
            addCriterion("creater >=", value, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterLessThan(String value) {
            addCriterion("creater <", value, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterLessThanOrEqualTo(String value) {
            addCriterion("creater <=", value, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterLike(String value) {
            addCriterion("creater like", value, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterNotLike(String value) {
            addCriterion("creater not like", value, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterIn(List<String> values) {
            addCriterion("creater in", values, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterNotIn(List<String> values) {
            addCriterion("creater not in", values, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterBetween(String value1, String value2) {
            addCriterion("creater between", value1, value2, "creater");
            return (Criteria) this;
        }

        public Criteria andCreaterNotBetween(String value1, String value2) {
            addCriterion("creater not between", value1, value2, "creater");
            return (Criteria) this;
        }
        
			
        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLike(Date value) {
            addCriterion("create_date like", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotLike(Date value) {
            addCriterion("create_date not like", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }
        
		
		 public Criteria andLikeQuery(BusOrder record) {
		 	List<String> list= new ArrayList<String>();
		 	List<String> list2= new ArrayList<String>();
        	StringBuffer buffer=new StringBuffer();
        	
        				 
			 if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			 list.add("ifnull(id,'')");
    		 }
    		 
						 
			 if(record.getShootingAddr()!=null&&StrUtil.isNotEmpty(record.getShootingAddr().toString())) {
    			 list.add("ifnull(shooting_addr,'')");
    		 }
    		 
						 
			 if(record.getShootingTime()!=null&&StrUtil.isNotEmpty(record.getShootingTime().toString())) {
    			 list.add("ifnull(shooting_time,'')");
    		 }
    		 
						 
			 if(record.getCustomerNum()!=null&&StrUtil.isNotEmpty(record.getCustomerNum().toString())) {
    			 list.add("ifnull(customer_num,'')");
    		 }
    		 
						 
			 if(record.getProps()!=null&&StrUtil.isNotEmpty(record.getProps().toString())) {
    			 list.add("ifnull(props,'')");
    		 }
    		 
						 
			 if(record.getTheme()!=null&&StrUtil.isNotEmpty(record.getTheme().toString())) {
    			 list.add("ifnull(theme,'')");
    		 }
    		 
						 
			 if(record.getCharge()!=null&&StrUtil.isNotEmpty(record.getCharge().toString())) {
    			 list.add("ifnull(charge,'')");
    		 }
    		 
						 
			 if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
    			 list.add("ifnull(status,'')");
    		 }
    		 
						 
			 if(record.getCreater()!=null&&StrUtil.isNotEmpty(record.getCreater().toString())) {
    			 list.add("ifnull(creater,'')");
    		 }
    		 
						 
			 if(record.getCreateDate()!=null&&StrUtil.isNotEmpty(record.getCreateDate().toString())) {
    			 list.add("ifnull(create_date,'')");
    		 }
    		 
									
			 if(record.getId()!=null&&StrUtil.isNotEmpty(record.getId().toString())) {
    			list2.add("'%"+record.getId()+"%'");
    		 }
        		 
						
			 if(record.getShootingAddr()!=null&&StrUtil.isNotEmpty(record.getShootingAddr().toString())) {
    			list2.add("'%"+record.getShootingAddr()+"%'");
    		 }
        		 
						
			 if(record.getShootingTime()!=null&&StrUtil.isNotEmpty(record.getShootingTime().toString())) {
    			list2.add("'%"+record.getShootingTime()+"%'");
    		 }
        		 
						
			 if(record.getCustomerNum()!=null&&StrUtil.isNotEmpty(record.getCustomerNum().toString())) {
    			list2.add("'%"+record.getCustomerNum()+"%'");
    		 }
        		 
						
			 if(record.getProps()!=null&&StrUtil.isNotEmpty(record.getProps().toString())) {
    			list2.add("'%"+record.getProps()+"%'");
    		 }
        		 
						
			 if(record.getTheme()!=null&&StrUtil.isNotEmpty(record.getTheme().toString())) {
    			list2.add("'%"+record.getTheme()+"%'");
    		 }
        		 
						
			 if(record.getCharge()!=null&&StrUtil.isNotEmpty(record.getCharge().toString())) {
    			list2.add("'%"+record.getCharge()+"%'");
    		 }
        		 
						
			 if(record.getStatus()!=null&&StrUtil.isNotEmpty(record.getStatus().toString())) {
    			list2.add("'%"+record.getStatus()+"%'");
    		 }
        		 
						
			 if(record.getCreater()!=null&&StrUtil.isNotEmpty(record.getCreater().toString())) {
    			list2.add("'%"+record.getCreater()+"%'");
    		 }
        		 
						
			 if(record.getCreateDate()!=null&&StrUtil.isNotEmpty(record.getCreateDate().toString())) {
    			list2.add("'%"+record.getCreateDate()+"%'");
    		 }
        		 
			        	
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append(" like CONCAT(");
        	
        	
        	buffer.append(StrUtil.join(",",list2));
        	
        	buffer.append(")");
        	if(!" CONCAT() like CONCAT()".equals(buffer.toString())) {
        		addCriterion(buffer.toString());
        	}
        	return (Criteria) this;
        }
        
        public Criteria andLikeQuery2(String searchText) {
		 	List<String> list= new ArrayList<String>();
		 
        	StringBuffer buffer=new StringBuffer();
        	
        	        	
    		list.add("ifnull(id,'')");
			
			        	
    		list.add("ifnull(shooting_addr,'')");
			
			        	
    		list.add("ifnull(shooting_time,'')");
			
			        	
    		list.add("ifnull(customer_num,'')");
			
			        	
    		list.add("ifnull(props,'')");
			
			        	
    		list.add("ifnull(theme,'')");
			
			        	
    		list.add("ifnull(charge,'')");
			
			        	
    		list.add("ifnull(status,'')");
			
			        	
    		list.add("ifnull(creater,'')");
			
			        	
    		list.add("ifnull(create_date,'')");
			
						
        	buffer.append(" CONCAT(");
	        buffer.append(StrUtil.join(",",list));
        	buffer.append(")");
        	buffer.append("like '%");
        	buffer.append(searchText);
        	buffer.append("%'");
        	
        	addCriterion(buffer.toString());
        	
        	return (Criteria) this;
        }
        
}
	
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}