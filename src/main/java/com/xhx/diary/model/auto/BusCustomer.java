package com.xhx.diary.model.auto;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.lang.Integer;

/**
 * 客户 BusCustomer 
 * @author xhx_自动生成
 * @email 1587211986@qq.com
 * @date 2020-12-05 00:14:26
 */
 @ApiModel(value="BusCustomer", description="客户")
public class BusCustomer implements Serializable {

	private static final long serialVersionUID = 1L;
	
		
	/**  **/
	@ApiModelProperty(value = "")
	private String id;
		
	/** 姓名 **/
	@ApiModelProperty(value = "姓名")
	private String name;
		
	/** 住址 **/
	@ApiModelProperty(value = "住址")
	private String address;
		
	/** 性别 **/
	@ApiModelProperty(value = "性别")
	private Integer gender;
		
	/** 手机 **/
	@ApiModelProperty(value = "手机")
	private String mobile;
		
	/** 邮箱 **/
	@ApiModelProperty(value = "邮箱")
	private String email;
		
	/** 职业 **/
	@ApiModelProperty(value = "职业")
	private String profession;
		
	/** 生日 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "生日")
	private Date birthday;
		
	/** 创建人 **/
	@ApiModelProperty(value = "创建人")
	private String creater;
		
	/** 创建时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
		
		
	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	 
			
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
	 
			
	public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
	 
			
	public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }
	 
			
	public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
	 
			
	public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
	 
			
	public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
	 
			
	public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
	 
			
	public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }
	 
			
	public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
	 
			
	public BusCustomer() {
        super();
    }
    
																																																				
	public BusCustomer(String id,String name,String address,Integer gender,String mobile,String email,String profession,Date birthday,String creater,Date createDate) {
	
		this.id = id;
		this.name = name;
		this.address = address;
		this.gender = gender;
		this.mobile = mobile;
		this.email = email;
		this.profession = profession;
		this.birthday = birthday;
		this.creater = creater;
		this.createDate = createDate;
		
	}
	
}