package com.xhx.diary.controller.gen;

import com.xhx.diary.shiro.util.ShiroUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageInfo;
import com.xhx.diary.common.base.BaseController;
import com.xhx.diary.common.domain.AjaxResult;
import com.xhx.diary.model.auto.BusOrder;
import com.xhx.diary.model.custom.TableSplitResult;
import com.xhx.diary.model.custom.Tablepar;
import com.xhx.diary.model.custom.TitleVo;
import com.xhx.diary.service.BusOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Date;

@Api(value = "约拍清单")
@Controller
@RequestMapping("/BusOrderController")
public class BusOrderController extends BaseController {

    private String prefix = "gen/busOrder";
    @Autowired
    private BusOrderService busOrderService;

    /**
     * 分页跳转
     */
    @ApiOperation(value = "分页跳转", notes = "分页跳转")
    @GetMapping("/view")
    @RequiresPermissions("gen:busOrder:view")
    public String view(ModelMap model) {
        String str = "约拍清单";
        setTitle(model, new TitleVo("列表", str + "管理", true, "欢迎进入" + str + "页面", true, false));
        return prefix + "/list";
    }

    /**
     * 分页查询
     */
    //@Log(title = "约拍清单集合查询", action = "111")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("gen:busOrder:list")
    @ResponseBody
    public Object list(Tablepar tablepar, BusOrder record) {
        PageInfo<BusOrder> page = busOrderService.list(tablepar, record);
        TableSplitResult<BusOrder> result = new TableSplitResult<BusOrder>(page.getPageNum(), page.getTotal(), page.getList());
        return result;
    }


    @ApiOperation(value = "", notes = "按月份查詢约单")
    @PostMapping("/selectOrderByMonth")
    @RequiresPermissions("gen:busOrder:list")
    @ResponseBody
    public Object selectOrderByMonth() {
        return busOrderService.selectOrderByMonth(ShiroUtils.getUserId());
    }

    /**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {
        return prefix + "/add";
    }

    /**
     * 新增
     */
    //@Log(title = "约拍清单新增", action = "111")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("gen:busOrder:add")
    @ResponseBody
    public AjaxResult add(BusOrder busOrder) {
        busOrder.setCreater(ShiroUtils.getUserId());
        busOrder.setCreateDate(new Date());
        int b = busOrderService.insertSelective(busOrder);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    //@Log(title = "约拍清单删除", action = "111")
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("gen:busOrder:remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        int b = busOrderService.deleteByPrimaryKey(ids);
        if (b > 0) {
            return success();
        } else {
            return error();
        }
    }

    /**
     * 检查Name
     *
     * @param
     * @return
     */
    @ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
    @PostMapping("/checkNameUnique")
    @ResponseBody
    public int checkNameUnique(BusOrder busOrder) {
        int b = busOrderService.checkNameUnique(busOrder);
        if (b > 0) {
            return 1;
        } else {
            return 0;
        }
    }


    /**
     * 修改跳转
     *
     * @param id
     * @param mmap
     * @return
     */
    @ApiOperation(value = "修改跳转", notes = "修改跳转")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        mmap.put("BusOrder", busOrderService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    //@Log(title = "约拍清单修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:busOrder:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BusOrder record) {
        return toAjax(busOrderService.updateByPrimaryKeySelective(record));
    }


    /**
     * 根据主键查询
     *
     * @param id
     * @param
     * @return
     */
    @ApiOperation(value = "根据id查询唯一", notes = "根据id查询唯一")
    @PostMapping("/get/{id}")
    public BusOrder edit(@PathVariable("id") String id) {
        return busOrderService.selectByPrimaryKey(id);
    }


}
