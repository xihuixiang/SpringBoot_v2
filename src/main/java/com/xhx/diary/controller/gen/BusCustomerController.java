package com.xhx.diary.controller.gen;

import com.xhx.diary.shiro.util.ShiroUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageInfo;
import com.xhx.diary.common.base.BaseController;
import com.xhx.diary.common.domain.AjaxResult;
import com.xhx.diary.model.auto.BusCustomer;
import com.xhx.diary.model.custom.TableSplitResult;
import com.xhx.diary.model.custom.Tablepar;
import com.xhx.diary.model.custom.TitleVo;
import com.xhx.diary.service.BusCustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "客户")
@Controller
@RequestMapping("/BusCustomerController")
public class BusCustomerController extends BaseController{
	
	private String prefix = "gen/busCustomer";
	@Autowired
	private BusCustomerService busCustomerService;
	
	/**
	 * 分页跳转
	 */
	@ApiOperation(value = "分页跳转", notes = "分页跳转")
	@GetMapping("/view")
	@RequiresPermissions("gen:busCustomer:view")
    public String view(ModelMap model)
    {	
		String str="客户";
		setTitle(model, new TitleVo("列表", str+"管理", true,"欢迎进入"+str+"页面", true, false));
        return prefix + "/list";
    }
	
	/**
	 * 分页查询
	 */
	//@Log(title = "客户集合查询", action = "111")
	@ApiOperation(value = "分页查询", notes = "分页查询")
	@PostMapping("/list")
	@RequiresPermissions("gen:busCustomer:list")
	@ResponseBody
	public Object list(Tablepar tablepar,BusCustomer record){
		PageInfo<BusCustomer> page=busCustomerService.list(tablepar,record) ; 
		TableSplitResult<BusCustomer> result=new TableSplitResult<BusCustomer>(page.getPageNum(), page.getTotal(), page.getList());
		System.out.println("========================"+ ShiroUtils.getUser().getId());
		return  result;
	}
	
	/**
     * 新增跳转
     */
    @ApiOperation(value = "新增跳转", notes = "新增跳转")
    @GetMapping("/add")
    public String add(ModelMap modelMap)
    {
        return prefix + "/add";
    }
	
	/**
     * 新增
     */
	//@Log(title = "客户新增", action = "111")
   	@ApiOperation(value = "新增", notes = "新增")
	@PostMapping("/add")
	@RequiresPermissions("gen:busCustomer:add")
	@ResponseBody
	public AjaxResult add(BusCustomer busCustomer){
		int b=busCustomerService.insertSelective(busCustomer);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 删除
	 * @param ids
	 * @return
	 */
	//@Log(title = "客户删除", action = "111")
	@ApiOperation(value = "删除", notes = "删除")
	@PostMapping("/remove")
	@RequiresPermissions("gen:busCustomer:remove")
	@ResponseBody
	public AjaxResult remove(String ids){
		int b=busCustomerService.deleteByPrimaryKey(ids);
		if(b>0){
			return success();
		}else{
			return error();
		}
	}
	
	/**
	 * 检查Name
	 * @param
	 * @return
	 */
	@ApiOperation(value = "检查Name唯一", notes = "检查Name唯一")
	@PostMapping("/checkNameUnique")
	@ResponseBody
	public int checkNameUnique(BusCustomer busCustomer){
		int b=busCustomerService.checkNameUnique(busCustomer);
		if(b>0){
			return 1;
		}else{
			return 0;
		}
	}
	
	
	/**
	 * 修改跳转
	 * @param id
	 * @param mmap
	 * @return
	 */
	@ApiOperation(value = "修改跳转", notes = "修改跳转")
	@GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("BusCustomer", busCustomerService.selectByPrimaryKey(id));

        return prefix + "/edit";
    }
	
	/**
     * 修改保存
     */
    //@Log(title = "客户修改", action = "111")
    @ApiOperation(value = "修改保存", notes = "修改保存")
    @RequiresPermissions("gen:busCustomer:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BusCustomer record)
    {
        return toAjax(busCustomerService.updateByPrimaryKeySelective(record));
    }

    
    /**
   	 * 根据主键查询
   	 * 
   	 * @param id
   	 * @param mmap
   	 * @return
   	 */
   	@ApiOperation(value = "根据id查询唯一", notes = "根据id查询唯一")
   	@PostMapping("/get/{id}")
   	public BusCustomer edit(@PathVariable("id") String id) {
   		return busCustomerService.selectByPrimaryKey(id);
   	}
    

	
}
