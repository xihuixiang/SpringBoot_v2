package com.xhx.diary.service;

import java.util.List;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.xhx.diary.common.base.BaseService;
import com.xhx.diary.common.support.ConvertUtil;
import com.xhx.diary.mapper.auto.BusCustomerMapper;
import com.xhx.diary.model.auto.BusCustomer;
import com.xhx.diary.model.auto.BusCustomerExample;
import com.xhx.diary.model.custom.Tablepar;
import com.xhx.diary.util.SnowflakeIdWorker;
import com.xhx.diary.util.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 客户 BusCustomerService
 * @Title: BusCustomerService.java 
 * @Package com.xhx.diary.service 
 * @author xhx_自动生成
 * @email 1587211986@qq.com
 * @date 2020-12-05 00:14:26  
 **/
@Service
public class BusCustomerService implements BaseService<BusCustomer, BusCustomerExample>{
	@Autowired
	private BusCustomerMapper busCustomerMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	 public PageInfo<BusCustomer> list(Tablepar tablepar,BusCustomer record){
	        BusCustomerExample testExample=new BusCustomerExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(record);
	        }
			//表格排序
			if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        }else{
	        	testExample.setOrderByClause("id ASC");
	        }
	        PageHelper.startPage(tablepar.getPageNum(), tablepar.getPageSize());
	        List<BusCustomer> list= busCustomerMapper.selectByExample(testExample);
	        PageInfo<BusCustomer> pageInfo = new PageInfo<BusCustomer>(list);
	        return  pageInfo;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
				
			List<String> lista=ConvertUtil.toListStrArray(ids);
			BusCustomerExample example=new BusCustomerExample();
			example.createCriteria().andIdIn(lista);
			return busCustomerMapper.deleteByExample(example);
			
				
	}
	
	
	@Override
	public BusCustomer selectByPrimaryKey(String id) {
				
			return busCustomerMapper.selectByPrimaryKey(id);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(BusCustomer record) {
		return busCustomerMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(BusCustomer record) {
				
		//添加雪花主键id
		record.setId(SnowflakeIdWorker.getUUID());
			
				
		return busCustomerMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(BusCustomer record, BusCustomerExample example) {
		
		return busCustomerMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(BusCustomer record, BusCustomerExample example) {
		
		return busCustomerMapper.updateByExample(record, example);
	}

	@Override
	public List<BusCustomer> selectByExample(BusCustomerExample example) {
		
		return busCustomerMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(BusCustomerExample example) {
		
		return busCustomerMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(BusCustomerExample example) {
		
		return busCustomerMapper.deleteByExample(example);
	}
	
	/**
	 * 检查name
	 * @param busCustomer
	 * @return
	 */
	public int checkNameUnique(BusCustomer busCustomer){
		BusCustomerExample example=new BusCustomerExample();
		example.createCriteria().andNameEqualTo(busCustomer.getName());
		List<BusCustomer> list=busCustomerMapper.selectByExample(example);
		return list.size();
	}


}
