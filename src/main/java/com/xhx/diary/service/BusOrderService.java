package com.xhx.diary.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import com.xhx.diary.model.vo.BusOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import cn.hutool.core.util.StrUtil;
import com.xhx.diary.common.base.BaseService;
import com.xhx.diary.common.support.ConvertUtil;
import com.xhx.diary.mapper.auto.BusOrderMapper;
import com.xhx.diary.model.auto.BusOrder;
import com.xhx.diary.model.auto.BusOrderExample;
import com.xhx.diary.model.custom.Tablepar;
import com.xhx.diary.util.SnowflakeIdWorker;
import com.xhx.diary.util.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 约拍清单 BusOrderService
 * @Title: BusOrderService.java 
 * @Package com.xhx.diary.service 
 * @author xhx_自动生成
 * @email 1587211986@qq.com
 * @date 2020-12-05 00:34:06  
 **/
@Service
public class BusOrderService implements BaseService<BusOrder, BusOrderExample>{
	@Autowired
	private BusOrderMapper busOrderMapper;
	
      	   	      	      	      	      	      	      	      	      	      	      	
	/**
	 * 分页查询
	 * @param
	 * @param
	 * @return
	 */
	 public PageInfo<BusOrder> list(Tablepar tablepar,BusOrder record){
	        BusOrderExample testExample=new BusOrderExample();
			//搜索
			if(StrUtil.isNotEmpty(tablepar.getSearchText())) {//小窗体
	        	testExample.createCriteria().andLikeQuery2(tablepar.getSearchText());
	        }else {//大搜索
	        	testExample.createCriteria().andLikeQuery(record);
	        }
			//表格排序
			if(StrUtil.isNotEmpty(tablepar.getOrderByColumn())) {
	        	testExample.setOrderByClause(StringUtils.toUnderScoreCase(tablepar.getOrderByColumn()) +" "+tablepar.getIsAsc());
	        }else{
	        	testExample.setOrderByClause("id ASC");
	        }
	        PageHelper.startPage(tablepar.getPageNum(), tablepar.getPageSize());
	        List<BusOrder> list= busOrderMapper.selectByExample(testExample);
	        PageInfo<BusOrder> pageInfo = new PageInfo<BusOrder>(list);
	        return  pageInfo;
	 }

	public List<BusOrderVo> selectOrderByMonth(String creater){
		 List<BusOrder> list= busOrderMapper.selectByCreater(creater);
		 List<BusOrderVo> busOrderVos=new ArrayList<>();
		 if(list!=null){
		 	list.forEach(o->{
				BusOrderVo busOrderVo=new BusOrderVo();
				busOrderVo.setClassName(o.getStatus()==0?"red":"green");
				busOrderVo.setStart(o.getShootingTime());
				busOrderVo.setTitle(o.getShootingAddr());
				busOrderVos.add(busOrderVo);
			});
		 }
		 return busOrderVos;
	 }

	@Override
	public int deleteByPrimaryKey(String ids) {
			List<String> lista=ConvertUtil.toListStrArray(ids);
			BusOrderExample example=new BusOrderExample();
			example.createCriteria().andIdIn(lista);
			return busOrderMapper.deleteByExample(example);
	}
	
	
	@Override
	public BusOrder selectByPrimaryKey(String id) {
				
			return busOrderMapper.selectByPrimaryKey(id);
				
	}

	
	@Override
	public int updateByPrimaryKeySelective(BusOrder record) {
		return busOrderMapper.updateByPrimaryKeySelective(record);
	}
	
	
	/**
	 * 添加
	 */
	@Override
	public int insertSelective(BusOrder record) {
		//添加雪花主键id
		record.setId(SnowflakeIdWorker.getUUID());
		return busOrderMapper.insertSelective(record);
	}
	
	
	@Override
	public int updateByExampleSelective(BusOrder record, BusOrderExample example) {
		
		return busOrderMapper.updateByExampleSelective(record, example);
	}

	
	@Override
	public int updateByExample(BusOrder record, BusOrderExample example) {
		
		return busOrderMapper.updateByExample(record, example);
	}

	@Override
	public List<BusOrder> selectByExample(BusOrderExample example) {
		
		return busOrderMapper.selectByExample(example);
	}

	
	@Override
	public long countByExample(BusOrderExample example) {
		
		return busOrderMapper.countByExample(example);
	}

	
	@Override
	public int deleteByExample(BusOrderExample example) {
		
		return busOrderMapper.deleteByExample(example);
	}
	
	/**
	 * 检查name
	 * @param busOrder
	 * @return
	 */
	public int checkNameUnique(BusOrder busOrder){
		BusOrderExample example=new BusOrderExample();
		example.createCriteria().andShootingAddrEqualTo(busOrder.getShootingAddr());
		List<BusOrder> list=busOrderMapper.selectByExample(example);
		return list.size();
	}


}
